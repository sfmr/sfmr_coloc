# sfmr_coloc

Generate SFMR <> satellite acquisition colocation product.

# Colocation methodology

For a given satellite acquisition product file, a given SFMR file and 
the associated cyclone track data :
- resample the SFMR data, mean values every 10 seconds
- translate SFMR each time-sparse data point onto the instantaneous acquisition
  grid. This is done by moving the SFMR data points according to the distance the 
  cyclone has traveled between the time of SFMR point acquisition and the time 
  of satellite acquisition.
  To sum up this operation : 
  new_sfmr_point_position = original_sfmr_point_position + 
  (cyclone_pos_at_sfmr_point_time - cyclone_pos_at_acquisition_time)
- For every translated SFMR point, check that a spatially close corresponding satellite
acquisition point exist. If it exists, the SFMR and satellite acquisition information
  for that point are stored and will be written in the product file with other points
  also matching that criteria.
  
A script will be in charge of performing these operations for a given trio besttrack-SFMR-satellite
Another script will take care of finding these trio and call the previous script for each one.