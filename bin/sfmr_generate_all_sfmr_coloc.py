#!/usr/bin/env python

import argparse
import sys
import logging

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from sfmr_coloc import generate_all_coloc

from sfmr_coloc import logger

if __name__ == "__main__":
    description = """
        Generate colocation products between a SFMR track and a satellite acquisition. The script uses the data from
        the CyclObs database to find all the SFMR and satellite data that can be colocated. For each match found, it
        will create a colocation product.
        """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-a", "--host_api", action="store", default="https://cyclobs.ifremer.fr")
    parser.add_argument("-d", "--days", action="store", type=int, default=None, help="Number of days to update")
    parser.add_argument("--debug", action="store_true", default=False, help="Run in debug mode (verbose output)")
    parser.add_argument("-o", "--out", action="store", type=str, required=True,
                        help="Output path in which write the colocation product files.")
    parser.add_argument("-f", "--sat_files", action="store", type=str, nargs="*", default=None,
                        help="Optionally specify specific satellite files for which to generate coloc")
    parser.add_argument("-i", "--input_dir", action="store", type=str, default=None,
                        help="Optionally specify a directory in which nclight files to colocate")
    parser.add_argument("-m", "--min_points", action="store", type=int, default=50,
                        help="Minimum colocation points required for generating a file.")
    parser.add_argument("-llgd", "--llgd", action="store_true",
                        default=False, help="Use SAR gridded with crs=4326 (lon/lat) files instead of "
                                            "gridded with azimutal equidistant projection files.")
    parser.add_argument("-sf", "--sfmr-source", action="store", default="all",
                        help="SFMR track source. HRD or NESDIS. Defaults to all.")
    parser.add_argument("-sm", "--sat-mission", action="store", default="all",
                        help="Satellite mission to use. Defaults to all.")
    parser.add_argument("-si", "--sat-instrument", action="store", default="all",
                        help="Satellite instrument to use. Defaults to all.")
    parser.add_argument("-st", "--sar-commit", action="store", type=str, required=False, default=None,
                        help="Override the default SAR commit from which to take SAR data.")
    parser.add_argument("-sc", "--sar-config", action="store", type=str, required=False, default=None,
                        help="Override the default SAR config from which to take SAR data.")

    args = parser.parse_args()

    if sys.gettrace():
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)

    if args.debug:
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        logging.basicConfig(level=logging.INFO)

    generate_all_coloc(args.out, args.host_api, args.days, args.min_points, args.sat_files, args.input_dir, args.llgd,
                       args.sfmr_source, args.sat_mission, args.sat_instrument, sar_commit=args.sar_commit,
                       sar_config=args.sar_config)
