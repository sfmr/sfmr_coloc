#!/bin/bash

set -e

while getopts ":o:a:" opt; do
  case $opt in
    o|path-out) # Option --path-out ou -o
      path_out="$OPTARG"
      ;;
    a|api-host) # Option --api-host ou -a
      api_host="$OPTARG"
      ;;
    \?) # En cas d'option non reconnue
      echo "Option invalide: -$OPTARG" >&2
      exit 1
      ;;
    :) # En cas d'option manquante
      echo "L'option -$OPTARG requiert un argument." >&2
      exit 1
      ;;
  esac
done

# Vérifier si les options sont présentes
if [ -z "$path_out" ] || [ -z "$api_host" ]; then
  echo "Usage: $0 [--path-out | -o] <chemin_de_sortie> [--api-host | -a] <hôte_API>"
  exit 1
fi

# Afficher les valeurs des options
echo "Chemin de sortie: $path_out"
echo "Hôte API: $api_host"

mkdir -p "$path_out/noaa_nesdis_star/RCM/"
mkdir -p "$path_out/noaa_aoml_hrd/RCM/"
mkdir -p "$path_out/noaa_nesdis_star/RS2/"
mkdir -p "$path_out/noaa_aoml_hrd/RS2/"
mkdir -p "$path_out/noaa_nesdis_star/S1A/"
mkdir -p "$path_out/noaa_aoml_hrd/S1A/"
mkdir -p "$path_out/noaa_nesdis_star/S1B/"
mkdir -p "$path_out/noaa_aoml_hrd/S1B/"

sfmr_generate_all_sfmr_coloc.py -a "$api_host" -o "$path_out/noaa_nesdis_star/RCM/" -sf NESDIS -sm RCM

sfmr_generate_all_sfmr_coloc.py -a "$api_host" -o "$path_out/noaa_aoml_hrd/RCM/" -sf HRD -sm RCM

sfmr_generate_all_sfmr_coloc.py -a "$api_host" -o "$path_out/noaa_nesdis_star/RS2/" -sf NESDIS -sm RS2

sfmr_generate_all_sfmr_coloc.py -a "$api_host" -o "$path_out/noaa_aoml_hrd/RS2/" -sf HRD -sm RS2

sfmr_generate_all_sfmr_coloc.py -a "$api_host" -o "$path_out/noaa_nesdis_star/S1A/" -sf NESDIS -sm S1A

sfmr_generate_all_sfmr_coloc.py -a "$api_host" -o "$path_out/noaa_aoml_hrd/S1A/" -sf HRD -sm S1A

sfmr_generate_all_sfmr_coloc.py -a "$api_host" -o "$path_out/noaa_nesdis_star/S1B/" -sf NESDIS -sm S1B

sfmr_generate_all_sfmr_coloc.py -a "$api_host" -o "$path_out/noaa_aoml_hrd/S1B/" -sf HRD -sm S1B