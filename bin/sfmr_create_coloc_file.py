#!/usr/bin/env python

import argparse
import sys
import logging
from sfmr_coloc import coloc_from_inputs
import pandas as pd

logger = logging.getLogger(__name__)

if __name__ == "__main__":
    description = """
        Generate one colocation product between a SFMR track and a satellite acquisition. BestTrack data is also
        needed for the colocation.
        
        BestTrack data can be given through several means. Either ..
        """
    pre_parser = argparse.ArgumentParser(add_help=False)
    pre_parser.add_argument("-v", "--version", action="store_true", help="Print version")
    pre_args, remaining_args = pre_parser.parse_known_args()

    from sfmr_coloc import __version__
    # Handle the version argument right away
    if pre_args.version:
        print(__version__)
        sys.exit()

    parser = argparse.ArgumentParser(description=description)

    #TODO be able to be completely independent from Cyclobs API

    parser.add_argument("-u", "--host_api", action="store", default="https://cyclobs.ifremer.fr")
    parser.add_argument("-t", "--track_file", action="store", default=None)
    parser.add_argument("--debug", action="store_true", default=False, help="Run in debug mode (verbose output)")
    parser.add_argument("-s", "--sfmr", action="store", type=str, required=True,
                        help="SFMR input file. Must be NetCDF.")
    parser.add_argument("-a", "--acq", action="store", type=str, required=True,
                        help="Satellite acquisition file input. Must be NetCDF.")
    parser.add_argument("-c", "--cyclone_sid", action="store", type=str, required=True,
                        help="Cyclone storm id")
    parser.add_argument("-m", "--mission_short", action="store", type=str, required=True,
                        help="The satellite mission in short format. Examples : S1A or S1B or SMOS or SMAP")
    parser.add_argument("-o", "--out", action="store", type=str, required=True,
                        help="Output path in which write the colocation product file.")
    parser.add_argument("-mp", "--min_points", action="store", type=int, default=50,
                        help="Minimum colocation points required for generating a file.")
    parser.add_argument("-v", "--version", action="store_true", help="Print version")

    args = parser.parse_args()

    if sys.gettrace():
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)

    if args.debug:
        logger.setLevel(logging.DEBUG)
        logging.basicConfig(level=logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        logging.basicConfig(level=logging.INFO)

    if args.track_file is not None:
        track_df = pd.read_csv(args.track_file)
    else:
        track_df = None

    coloc_from_inputs(sfmr_file=args.sfmr, sat_file=args.acq, mission_short=args.mission_short, sid=args.cyclone_sid,
                      out_path=args.out, host_api=args.host_api, track_df=track_df, min_points=args.min_points)
