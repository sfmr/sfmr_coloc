import glob
import os

from setuptools import setup, find_packages

install_requires = [
      'pandas',
      'geopandas',
      'rasterio',
      'affine',
      'pyproj',
      'numpy',
      'psutil',
      'xarray',
      'rioxarray',
      'numba',
      'netcdf4',
      "cyclobs_utils",
      'pathurl>=0.0.dev0'
]

setup(name='sfmr_coloc',
      description='Generate files products for the colocation between SFMR and satellite aquisitions.',
      url='https://gitlab.ifremer.fr/sfmr/sfmr_coloc',
      author="Théo Cévaër",
      author_email="tcevaer@ifremer.fr",
      use_scm_version={'write_to': '%s/_version.py' % "sfmr_coloc"},
      license='GPL',
      scripts=[f for f in glob.glob('bin/**') if not os.path.isdir(f)],
      packages=find_packages(),
      include_package_data=True,
      setup_requires=['setuptools_scm'],
      install_requires=install_requires,
      zip_safe=False
      )
