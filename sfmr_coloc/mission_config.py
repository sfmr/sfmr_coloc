import numpy as np
import logging
import os

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)


def get_lon(sat_ds):
    if "x" in sat_ds.coords:
        return "x"
    elif "lon" in sat_ds.coords:
        return "lon"
    elif "longitude" in sat_ds.coords:
        return "longitude"


def get_lat(sat_ds):
    if "y" in sat_ds.coords:
        return "y"
    elif "lon" in sat_ds.coords:
        return "lat"
    elif "longitude" in sat_ds.coords:
        return "latitude"


class MissionConfig:
    def __init__(
        self,
        instrument,
        sfmr_resample_freq_sec,
        acquisition_time_attribute,
        datetime_format,
        var_mapping,
        pixel_spacing,
        sfmr_spatial_downsample_dist,
        max_coloc_dist,
    ):
        self.instrument = instrument
        self.sfmr_resample_freq_sec = sfmr_resample_freq_sec
        self.acquisition_time_attribute = acquisition_time_attribute
        self.datetime_format = datetime_format
        self.var_mapping = var_mapping
        self.pixel_spacing = pixel_spacing
        self.sfmr_spatial_downsample_dist = sfmr_spatial_downsample_dist
        self.max_coloc_dist = max_coloc_dist

    def prepare_vars(self, sat_ds):
        msg = "Error, this abstract function should not be used"
        logger.error(msg)
        raise NotImplementedError(msg)

    def clean_data(self, sat_ds):
        msg = "Error, this abstract function should not be used"
        logger.error(msg)
        raise NotImplementedError(msg)


class SARConfig(MissionConfig):
    def __init__(self):
        # For the var_mapping parameter, "sat_wind_speed" is required for one entry
        super().__init__(
            instrument="C-Band Synthetic Aperture Radar",
            sfmr_resample_freq_sec=10,
            acquisition_time_attribute="measurementDate",
            datetime_format="%Y-%m-%dT%H:%M:%S%z",
            var_mapping={
                "wind_speed": "sat_wind_speed",
                "owiWindSpeed_co": "sat_wind_speed_co",
                "owiWindSpeed_cross": "sat_wind_speed_cross",
                "nrcs_detrend_co": "sat_nrcs_detrend_co",
                "nrcs_co": "sat_nrcs_co",
                "nrcs_detrend_cross": "sat_nrcs_detrend_cross",
                "nrcs_cross": "sat_nrcs_cross",
                "incidence_angle": "sat_incidence_angle",
                # "zonal_wind_from_direction": "sat_zonal_wind_from_direction",
                # "meridional_wind_from_direction": "sat_meridional_wind_from_direction",
                "zonal_wind_streaks_orientation": "sat_zonal_wind_streaks_orientation",
                "meridional_wind_streaks_orientation": "sat_meridional_wind_streaks_orientation",
                "wind_speed_ecmwf": "sat_wind_speed_ecmwf",
                "wind_direction_ecmwf": "sat_wind_direction_ecmwf",
                "nesz_cross": "sat_nesz_cross",
                "nesz_co": "sat_nesz_co",
                "heading_angle": "sat_heading__angle",
                "nesz_cross_corrected": "sat_nesz_cross_corrected",
                "nesz_cross_mean": "sat_nesz_cross_mean",
            },
            pixel_spacing=None,
            sfmr_spatial_downsample_dist=0.03,
            max_coloc_dist=1000,
        )

    def prepare_vars(self, sat_ds):
        lon = get_lon(sat_ds)
        lat = get_lat(sat_ds)

        if "wind_from_direction" in sat_ds.data_vars:
            u = np.cos(
                np.deg2rad(360 - sat_ds["wind_from_direction"].values - 90)
            )  # zonal
            v = np.sin(
                np.deg2rad(360 - sat_ds["wind_from_direction"].values - 90)
            )  # merid
            sat_ds["zonal_wind_from_direction"] = (("time", lat, lon), u)
            sat_ds["zonal_wind_from_direction"].attrs = {
                "long_name": "Zonal component of wind from direction (meteorologic convention)."
            }
            sat_ds["meridional_wind_from_direction"] = (("time", lat, lon), v)
            sat_ds["meridional_wind_from_direction"].attrs = {
                "long_name": "Meridional component of wind from direction (meteorologic convention)."
            }

        if "wind_streaks_orientation" not in sat_ds.data_vars:
            return sat_ds

        u = np.cos(
            np.deg2rad(360 - sat_ds["wind_streaks_orientation"].values - 90)
        )  # zonal
        v = np.sin(
            np.deg2rad(360 - sat_ds["wind_streaks_orientation"].values - 90)
        )  # merid
        sat_ds["zonal_wind_streaks_orientation"] = (("time", lat, lon), u)
        sat_ds["zonal_wind_streaks_orientation"].attrs = {
            "long_name": "Zonal component of estimation of wind streaks orientation."
        }
        sat_ds["meridional_wind_streaks_orientation"] = (("time", lat, lon), v)
        sat_ds["meridional_wind_streaks_orientation"].attrs = {
            "long_name": "Meridional component of estimation of wind streaks orientation."
        }

        return sat_ds

    def clean_data(self, sat_ds):
        sat_ds = sat_ds.where(sat_ds["mask_flag"] == 0)
        return sat_ds


class LBandConfig(MissionConfig):
    def __init__(self):
        # For the var_mapping parameter, "sat_wind_speed" is required for one entry
        super().__init__(
            instrument="L-Band Radiometer",
            sfmr_resample_freq_sec=10,
            acquisition_time_attribute="measurementStartDate",
            datetime_format="%Y-%m-%d %H:%M:%S",
            var_mapping={"wind_speed": "sat_wind_speed"},
            pixel_spacing=0.25,
            sfmr_spatial_downsample_dist=0.25,
            max_coloc_dist=27000,
        )

    def prepare_vars(self, sat_ds):
        return sat_ds

    def clean_data(self, sat_ds):
        return sat_ds
