from cyclobs_utils.cyclobs_config import SAR_INSTRUMENT_SHORT, INSTRUMENTS_MISSIONS_SHORT, LBAND_INSTRUMENT_SHORT

SAR_missions = INSTRUMENTS_MISSIONS_SHORT[SAR_INSTRUMENT_SHORT]
l_band_missions = INSTRUMENTS_MISSIONS_SHORT[LBAND_INSTRUMENT_SHORT]

# SFMR source noaa_aoml_hrd
HRD = "HRD"
# SFMR source noaa_nesdis_star
NESDIS = "NESDIS"

# Map sfmr variables depending on source to standardize access
sfmr_sources_variables = {
    NESDIS: {"time": "time", "lon": "longitude", "lat": "latitude", "wind_speed": "wind_speed", "quality": "quality"},
    HRD: {"time": "time", "lon": "LON", "lat": "LAT", "wind_speed": "SWS", "quality": "FLAG"}
}

# Map sfmr variables to new name in final product
sfmr_var_mapping = {
    NESDIS: {
        "wind_speed": "sfmr_wind_speed",
        "rain_rate": "sfmr_rain_rate",
        "altitude": "sfmr_altitude",
        "time": "sfmr_time",
        # "translation_speed": "sfmr_translation_speed",
        "longitude": "sfmr_lon",
        "latitude": "sfmr_lat"
    },
    HRD: {
        "SWS": "sfmr_wind_speed",
        "LON": "sfmr_lon",
        "LAT": "sfmr_lat",
        # "translation_speed": "sfmr_translation_speed",
        "RALT": "sfmr_altitude",
        "RANG": "sfmr_roll_angle",
        "PANG": "sfmr_pitch_angle",
        "ATEMP": "sfmr_air_temperature",
        "SST": "sfmr_sea_surface_temperature",
        "SALN": "sfmr_salinity",
        "SRR": "sfmr_rain_rate",
        "FWS": "sfmr_flight_level_wind_speed",
        "FDIR": "sfmr_flight_level_wind_direction",
        "time": "sfmr_time"
    }
}
# Helper dict to find source sfmr variable name from final name
sfmr_var_mapping_reversed = {
    HRD: dict((v, k) for k, v in sfmr_var_mapping[HRD].items()),
    NESDIS: dict((v, k) for k, v in sfmr_var_mapping[NESDIS].items())
}

# SFMR variables excluded from resampling
sfmr_time_resample_excluded = ["sfmr_time", "sfmr_lon", "sfmr_lat"]