import glob
import os

from setuptools import setup, find_packages

setup(name='sfmr_coloc',
      description='Generate files products for the colocation between SFMR and satellite aquisitions.',
      url='https://gitlab.ifremer.fr/sfmr/sfmr_coloc',
      author="Théo Cévaër",
      author_email="tcevaer@ifremer.fr",
      license='GPL',
      scripts=[f for f in glob.glob('bin/**') if not os.path.isdir(f)],
      packages=find_packages(),
      include_package_data=True,
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      zip_safe=False
      )
